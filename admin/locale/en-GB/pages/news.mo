��    ;      �  O   �           	          *     7     E     S     g     ~     �     �     �     �     �     �     �     �     �            
        *     8     D     V     f     t     �     �     �     �     �     �     �     �     �               )     =     N     \  
   i     t     }     �     �     �     �     �     �     �     �     �          ,     D     V     m  (   }     �     �     �     �     �  %   �  )   	  #   C	  (   g	  %   �	     �	     �	     �	  	   �	     �	     �	     
     
     
     )
     0
     D
     R
  
   ^
     i
     y
     �
  
   �
     �
     �
  
   �
  F   �
          %  y   6     �     �     �     �  
   �     �     �               :     G     K  
   d  �   o     
  .     !   G  ,   i  .   �  !   �  )   �  )        ;                             0      /      +         ;   -             $   %   2       *          ,   &          8   "                    9       )                  	                        5   7   (   4      #                   '   1       .      3   !          
         6       :        answerComment authorNotAvailable commentReply deleteComment displayedName errorAtLeastOneNews errorAtLeastOneNewsCat errorNewsCatFirst errorNewsCatNotFound errorNewsImageDelete goOnEdit mainCategory newAdd newEdit news newsActivate newsAlreadyExists newsArchive newsArchiveLast newsAuthor newsCatCreate newsCatEdit newsCatLastUpdate newsCatOverview newsCatParent newsCatSort newsCommentActivate newsCommentAnswerEdit newsCommentEdit newsComments newsDate newsDeleteCat newsDeleteComment newsDeleteNews newsDesc newsInfinite newsMandatoryFields newsMetaDescription newsMetaKeywords newsMetaTitle newsOverview newsPicAdd newsPics newsPraefix newsPreviewText newsSeo newsURL newsValidation noNewsAuthor selectAuthor successNewsCatDelete successNewsCatSave successNewsCommentDelete successNewsCommentUnlock successNewsCommmentEdit successNewsDelete successNewsImageDelete successNewsSave Content-Type: text/plain; charset=UTF-8
 Reply comment Not available Reply to comment Delete comments? Displayed name Please select at least one blog post. Please select at least one blog category. Please enter a blog category first. Could not find blog category with ID %d. Could not delete selected blog image.  and continue editing Main category Create new post Edit post Blog Activate already exists. Archive Archived posts Author Create new category Edit category Last update Categories Parent category Category sorting Unactivated comments Edit reply Editing comments Comments Created on The following blog categories and their subcategories will be deleted: Delete comment Delete blog post Use the JTL-Shop blog to keep your customers up to date. Create new blog posts and specify how posts are to be displayed. always (*) mandatory field Meta description Meta keywords Meta title Posts Add another image Available images Prefix for monthly overview Preview text SEO https://jtl-url.de/7vfnm Created on No author available. If you want to explicitly specify an author for this post, please create a corresponding user with access rights for the blog system. Select author Selected blog categories deleted successfully. Blog category added successfully. Selected blog comments deleted successfully. Selected blog comments activated successfully. Blog comment edited successfully. Selected blog posts deleted successfully. Selected blog image deleted successfully. Blog post saved successfully. 